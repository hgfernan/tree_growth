#! /usr/bin/env python3

import sys    # used for command line parsing 
import random # used for random number generation


class Tree:
    def __init__ (self, pap_90, pap_95):
        self.pap_90 = pap_90
        self.pap_95 = pap_95
        self.incr   = pap_95 - pap_90

        return

    def get_pap_90 (self):
        return (self.pap_90)        

    def get_pap_95 (self):
        return (self.pap_95)        

    def get_incr (self):
        return (self.incr)        

    def print_tree (self):
        fmt = "pap_90 {0:f}, pap 95 {1:f}, incr {2:f}"
        items = (self.get_pap_90 (), self.get_pap_95 (), self.get_incr ())
        print(fmt.format(*items))

        return

class Tree_Tab:
    def __init__ (self, seed):
        self.l     = 3
        self.sel   = 0
        self.tab   = []
        self.start = 0

        self.history = []

        self.seed = seed

        return

    def get_input (self, fname):
        # TODO: parameter checking

        try:
            f = open (fname, "r")
        except IOError as e:
            print(e)
            return 1

        # read it till the end
        s = f.readline ()
        lineno = 1
        while (s != ''): 
            line = s.split ()
            
            # confirm all items were found in line 
            if (len (line) < 2):
                print("Too few arguments in line {d}".format(lineno))
                f.close ()

                return (1)

            # confirm all items found are numeric
            pap_90 = None
            pap_95 = None

            try:
                pap_90 = int (line[0])
                pap_95 = int (line[1])
            except ValueError as e:
                print("Wrong input in line %d: '%s'" % (lineno, line))
                print(e)

                return (1)
 
            # create a tree object
            t = Tree (pap_90, pap_95)

            # append tree to the tab  
            self.append (t)

            # read the new line or die trying
            s = f.readline ()
            lineno = lineno + 1

        # close the data source
        f.close ()

        # normal function termination 
        return (0)

    def get_len (self):
        return (len (self.tab)) 

    def get_last (self):
        if (self.tab == []):
            return (None)
    
        return (self.tab[self.get_len () - 1]) 

    def get_at (self, ind):
        # TODO: parameter checking

        return (self.tab[ind])

    def set_at (self, ind, t):
        # TODO: parameter checking

        self.tab[ind] = t
        return (0)

    def append (self, t):
        # TODO: parameter checking
        
        self.tab.insert (len (self.tab), t)

        # normal function termination
        return (0)

    def sort_tab (self):
        # sorts vector using Bubble (shame! :)) method

        # TODO: make sure tab is a valid vector

        for i in range (len (self.tab) - 1):
            j = i + 1
            while (j < len (self.tab)):
                if (self.tab[i].pap_90 > self.tab[j].pap_90):
                    tmp = self.tab[i]
                    self.tab[i] = self.tab[j]
                    self.tab[j] = tmp 
                j = j + 1
        return (0)
        
    def get_l (self):
        return (self.l)
        
    def set_l (self, l):
        # TODO: parameter checking
        
        self.l = l

        return (0) 
        
    def get_start (self):
        return (self.start)
        
    def set_start (self, start):
        # TODO: parameter checking
        
        self.start = start

        return (0) 
        
    def get_sel (self):
        return (self.sel)

    def go_1st (self):
        # TODO: check tab size, l, start and seed are OK
    
        # set seed 
        random.seed (self.seed)
    
        # select a tree
        self.sel = int ( round (random.uniform (0, self.l) ))

        # TODO: check whether selected index is valid

        # add trunk values to it
        self.history.insert (0, self.tab[self.sel].pap_90)
        self.history.insert (1, self.tab[self.sel].pap_95)

        return (0)
    
    def get_history (self):
        return (self.history)

    def go_next (self):
        # TODO: see if it can be done

        # find new center
        target = self.history[len (self.history) - 1]
        min_diff = abs (target - self.tab[0].pap_90)
        min_ind  = 0

        for ind in range (1, len (self.tab)):
            diff = abs (target - self.tab[ind].pap_90)
            if (diff < min_diff):
                min_ind = ind
                min_diff = diff
    
        # set new start
        self.start = min_ind - (self.l / 2)
        last = min_ind + (self.l / 2)
        if ((self.l & 1)): 
            last = last + 1

        if (self.start < 0):
            self.start = 0


        if (last >= len (self.tab)):
            last = len (self.tab) - 1 

        # find new selected index
        self.sel = int ( round (random.uniform (self.start, last) ))

        # insert increment to history 
        self.history.insert (len (self.history), target + self.tab[self.sel].incr)
 
        # normal function termination
        return (0)

    def get_tallest (self):
        t = self.tab[len (self.tab) - 1]

        return (t)

    def get_max_height (self):
        # TODO: confirm tallest is a valid tree 

        return (self.get_tallest ().pap_90)

    def get_history_top (self):
        # TODO: confirm state allows that

        return (self.history[len (self.history) - 1])

def main(argv):
    print("Building tree")
    
    tt = Tree_Tab ((1 << 23) - 1)
    
    # if there's no file name to input from, use wired data
    if (len (sys.argv) == 1):
        t = Tree(27, 37)
        t.print_tree()
        tt.append (t)
    
        t = Tree (26, 41)
        t.print_tree()
        tt.append (t)
    
        t = Tree (26, 32)
        t.print_tree()
        tt.append (t)
    
        t = Tree (27, 49)
        t.print_tree()
        tt.append (t)
    
        t = Tree (28, 51)
        t.print_tree()
        tt.append (t)
    
        t = Tree (67, 86)
        t.print_tree()
        tt.append (t)
    
    # suppose 2nd arg is data file 
    else:
        if (tt.get_input (sys.argv[1])):
            fmt = "{}: could not input from file '{}'"
            print( fmt.format(sys.argv[0], sys.argv[1]) )    
    
            sys.exit (1)
    
    print("Printing tree tab")
    for ind in range (tt.get_len ()):
        print("ind {}:".format(ind))
        tt.get_at(ind).print_tree()
    
    print("Will sort")
    tt.sort_tab()
    
    print("Printing tree tab")
    for ind in range (tt.get_len()):
        print("ind {}:".format(ind))
        tt.get_at(ind).print_tree ()
    
    tt.go_1st ()
    
    print( "Selected tree {}".format(tt.get_sel ()) )
    h = tt.get_history ()
    print("History: {}".format(h))
    
    print( "\nHistory top:  {0:f}".format(tt.get_history_top ()) )
    print( "Maximum height: %f" % (tt.get_max_height ()) )
    
    while (tt.get_history_top() < tt.get_max_height()):
        print("Go next")
    
        tt.go_next ()
    
        print( "Selected tree %d" % (tt.get_sel ()) )
        h = tt.get_history ()
        print("History: %s" % (h))
    
if __name__ == '__main__':
    sys.exit(main(sys.argv))